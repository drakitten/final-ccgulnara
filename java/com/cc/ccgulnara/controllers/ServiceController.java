package com.cc.ccgulnara.controllers;

import com.cc.ccgulnara.models.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ServiceController {

    private final com.cc.ccgulnara.services.Service serviceService;

    public ServiceController(com.cc.ccgulnara.services.Service serviceService) {
        this.serviceService = serviceService;
    }

    @GetMapping(path = "/services")
    public List<Service> getAllServices() {
        return serviceService.getAll();
    }

    @GetMapping(path = "/services/{id}")
    public Service getServiceById(@PathVariable int id){
        return (Service) serviceService.getById(id);
    }

    @PostMapping(path = "/services")
    public void createCustomer(@RequestBody Service service) {
        serviceService.create(service);
    }

    @PostMapping(path = "/services/{id}")
    public String deleteCustomerById(@PathVariable int id) {
        serviceService.deleteById(id);
        return "Service is deleted!";
    }

}
