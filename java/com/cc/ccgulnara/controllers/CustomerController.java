package com.cc.ccgulnara.controllers;

import com.cc.ccgulnara.models.Customer;
import com.cc.ccgulnara.services.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CustomerController {

    private final Service customerService;

    public CustomerController(Service customerService) {
        this.customerService = customerService;
    }

    @GetMapping(path = "/customers")
    public List<Customer> getAllCustomers() {
        return customerService.getAll();
    }

    @GetMapping(path = "/customers/{id}")
    public Customer getCustomerById(@PathVariable int id){
        return (Customer) customerService.getById(id);
    }

    @PostMapping(path = "/customers")
    public void createCustomer(@RequestBody Customer customer) {
        customerService.create(customer);
    }

    @DeleteMapping(path = "/customers/{id}")
    public String deleteCustomerById(@PathVariable int id) {
        customerService.deleteById(id);
        return "Customer is deleted!";
    }
}
