package com.cc.ccgulnara.controllers;

import com.cc.ccgulnara.models.Tools;
import com.cc.ccgulnara.services.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ToolsController {

    private final Service toolsService;

    public ToolsController(Service toolsService) {
        this.toolsService = toolsService;
    }

    @GetMapping(path = "/tools")
    public List<Tools> getAllTools() {
        return toolsService.getAll();
    }

    @GetMapping(path = "/tools/{id}")
    public Tools getToolsById(@PathVariable int id){
        return (Tools) toolsService.getById(id);
    }

    @PostMapping(path = "/tools")
    public void createTools(@RequestBody Tools tools) {
        toolsService.create(tools);
    }

    @PostMapping(path = "/tools/{id}")
    public String deleteToolsById(@PathVariable int id) {
        toolsService.deleteById(id);
        return "Tools are deleted!";
    }
}
