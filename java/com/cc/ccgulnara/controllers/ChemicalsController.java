package com.cc.ccgulnara.controllers;

import com.cc.ccgulnara.models.Chemicals;
import com.cc.ccgulnara.services.ChemicalsService;
import com.cc.ccgulnara.services.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ChemicalsController {

    private final Service chemicalsService;

    public ChemicalsController(Service chemicalsService) {
        this.chemicalsService = chemicalsService;
    }

    @GetMapping(path = "/chemicals")
    public List<Chemicals> getAllChemicals() {
        return chemicalsService.getAll();
    }

    @GetMapping(path = "/chemicals/{id}")
    public Chemicals getChemicalsById(@PathVariable int id){
        return (Chemicals) chemicalsService.getById(id);
    }

    @PostMapping(path = "/chemicals")
    public void createChemicals(@RequestBody Chemicals chemicals) {
        chemicalsService.create(chemicals);
    }

    @PostMapping(path = "/chemicals/{id}")
    public String deleteChemicalsById(@PathVariable int id) {
        chemicalsService.deleteById(id);
        return "Chemicals are deleted!";
    }
}
