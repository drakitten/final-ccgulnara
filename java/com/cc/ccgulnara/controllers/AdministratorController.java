package com.cc.ccgulnara.controllers;

import com.cc.ccgulnara.models.Administrator;
import com.cc.ccgulnara.services.AdministratorService;
import com.cc.ccgulnara.services.Service;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AdministratorController {

    private final Service administratorService;

    public AdministratorController(Service administratorService) {
        this.administratorService = administratorService;
    }

    @GetMapping(path = "/administrators")
    public List<Administrator> getAllAdmins() {
        return administratorService.getAll();
    }

    @GetMapping(path = "/administrators/{id}")
    public Administrator getAdministratorById(@PathVariable int id){
        return (Administrator) administratorService.getById(id);
    }

    @PostMapping(path = "/administrators")
    public void createAdministrator(@RequestBody Administrator administrator) {
        administratorService.create(administrator);
    }

    @PostMapping(path = "/administrators/{id}")
    public String deleteAdministratorById(@PathVariable int id) {
        administratorService.deleteById(id);
        return "Administrator is deleted!";
    }
}
