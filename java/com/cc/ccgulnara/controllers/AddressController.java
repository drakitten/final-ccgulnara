package com.cc.ccgulnara.controllers;

import com.cc.ccgulnara.models.Address;
import com.cc.ccgulnara.services.AddressService;
import com.cc.ccgulnara.services.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AddressController {

    private final Service addressService;

    public AddressController(Service addressService) {
        this.addressService = addressService;
    }

    @GetMapping(path = "/addresses")
    public List<Address> getAllAddresses() {
        return addressService.getAll();
    }

    @GetMapping(path = "/addresses/{id}")
    public Address getAddressById(@PathVariable int id){
        return (Address) addressService.getById(id);
    }

    @PostMapping(path = "/addresses")
    public String createAddress(@RequestBody Address address) {
        addressService.create(address);
        return "Address is added!";
    }

    @PostMapping(path = "/addresses/{id}")
    public String deleteAddressById(@PathVariable int id) {
        addressService.deleteById(id);
        return "Address is deleted!";
    }
}
