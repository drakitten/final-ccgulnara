package com.cc.ccgulnara.controllers;

import com.cc.ccgulnara.models.Orders;
import com.cc.ccgulnara.services.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class OrdersController {

    private final Service ordersService;

    public OrdersController(Service ordersService) {
        this.ordersService = ordersService;
    }

    @GetMapping(path = "/orders")
    public List<Orders> getAllOrders() {
        return ordersService.getAll();
    }

    @GetMapping(path = "/orders/{id}")
    public Orders getOrderById(@PathVariable int id) {
        return (Orders) ordersService.getById(id);
    }

    @PostMapping(path = "/orders")
    public String createOrder(Orders orders){
        ordersService.create(orders);
        return "Order is created!";
    }

    @PostMapping(path = "/orders/{id}")
    public String deleteOrderById(@PathVariable int id) {
        ordersService.deleteById(id);
        return "Order is deleted!";
    }
}
