package com.cc.ccgulnara.models;

public class Service {

    private int service_id;
    private String s_name;
    private float duration_hours;
    private int ch_group_id;
    private int tool_group_id;


    public int getService_id() {
        return service_id;
    }

    public void setService_id(int service_id) {
        this.service_id = service_id;
    }

    public String getS_name() {
        return s_name;
    }

    public void setS_name(String s_name) {
        this.s_name = s_name;
    }

    public float getDuration_hours() {
        return duration_hours;
    }

    public void setDuration_hours(float duration_hours) {
        this.duration_hours = duration_hours;
    }

    public int getCh_group_id() {
        return ch_group_id;
    }

    public void setCh_group_id(int ch_group_id) {
        this.ch_group_id = ch_group_id;
    }

    public int getTool_group_id() {
        return tool_group_id;
    }

    public void setTool_group_id(int tool_group_id) {
        this.tool_group_id = tool_group_id;
    }
}
