package com.cc.ccgulnara.models;

public class Tools {

    private int tool_group_id;
    private String tool_group_name;

    public int getTool_group_id() {
        return tool_group_id;
    }

    public void setTool_group_id(int tool_group_id) {
        this.tool_group_id = tool_group_id;
    }

    public String getTool_group_name() {
        return tool_group_name;
    }

    public void setTool_group_name(String tool_group_name) {
        this.tool_group_name = tool_group_name;
    }
}
