package com.cc.ccgulnara.models;

public class Chemicals {

    private int ch_group_id;
    private String ch_group_name;

    public int getCh_group_id() {
        return ch_group_id;
    }

    public void setCh_group_id(int ch_group_id) {
        this.ch_group_id = ch_group_id;
    }

    public String getCh_group_name() {
        return ch_group_name;
    }

    public void setCh_group_name(String ch_group_name) {
        this.ch_group_name = ch_group_name;
    }
}
