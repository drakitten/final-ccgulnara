package com.cc.ccgulnara.services;

import com.cc.ccgulnara.models.Address;
import com.cc.ccgulnara.repositories.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AddressService implements com.cc.ccgulnara.services.Service<Address> {

    public Logger logger = LoggerFactory.getLogger(CustomerService.class);

    private Repository addressRepository;

    public AddressService(Repository addressRepository) {
        this.addressRepository = addressRepository;
    }

    public List<Address> getAll() {
        try {
            return addressRepository.getAll();
        } catch (RuntimeException e) {
            logger.error("Error: "+e.getMessage());
        }
        return null;
    }

    public Address getById(int id) {
        Address address = new Address();
        try {
            return (Address) addressRepository.getById(id);
        } catch (RuntimeException e) {
            logger.error("Error: "+e.getMessage());
            address.setAddress_id(404);
            address.setCountry("Error:");
            address.setCity("We do not have address by that id!");
        }
        return address;
    }

    public void create(Address address) {
        try {
            addressRepository.create(address);
        } catch (RuntimeException e) {
            logger.error("Error: "+e.getMessage());
        }
    }

    public void deleteById(int id) {
        try {
            addressRepository.deleteById(id);
        } catch (RuntimeException e) {
            logger.error("Error: "+e.getMessage());
        }
    }
}
