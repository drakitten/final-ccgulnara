package com.cc.ccgulnara.services;

import com.cc.ccgulnara.models.Customer;

import java.util.List;

public interface Service<T> {

    List<T> getAll();
    <T> T getById(int id);
    void create(T item);
    void deleteById(int id);
}
