package com.cc.ccgulnara.services;

import com.cc.ccgulnara.SpringException;
import com.cc.ccgulnara.models.Customer;
import com.cc.ccgulnara.repositories.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceService implements com.cc.ccgulnara.services.Service<com.cc.ccgulnara.models.Service> {

    public Logger logger = LoggerFactory.getLogger(CustomerService.class);

    private Repository serviceRepository;

    public ServiceService(Repository serviceRepository) {
        this.serviceRepository = serviceRepository;
    }

    public List<com.cc.ccgulnara.models.Service> getAll() {
        try {
            return serviceRepository.getAll();
        } catch (RuntimeException e) {
            logger.error("Error: "+e.getMessage());
        }
        return null;
    }

    public com.cc.ccgulnara.models.Service getById(int id) {
        com.cc.ccgulnara.models.Service service = new com.cc.ccgulnara.models.Service();
        try {
            return (com.cc.ccgulnara.models.Service) serviceRepository.getById(id);
        } catch (RuntimeException e) {
            logger.error("Error: "+e.getMessage());
            service.setService_id(404);
            service.setS_name("We do not have that service!");
        }
        return service;
    }

    public void create(com.cc.ccgulnara.models.Service service) {
        try {
            if (service.getS_name().length() < 1) {
                //exception handling
                throw new SpringException("Given name is too short");
            } else {
                try {
                    serviceRepository.create(service);
                } catch (RuntimeException e) {
                    logger.error("Error: "+e.getMessage());
                }
            }
        } catch (SpringException ex) {
            logger.error("Error: "+ex.getExceptionMsg());
        }
    }

    public void deleteById(int id) {
        try {
            serviceRepository.deleteById(id);
        } catch (RuntimeException e) {
            logger.error("Error: "+e.getMessage());
        }
    }

}
