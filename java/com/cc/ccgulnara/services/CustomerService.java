package com.cc.ccgulnara.services;

import com.cc.ccgulnara.SpringException;
import com.cc.ccgulnara.models.Customer;
import com.cc.ccgulnara.repositories.Repository;
import org.postgresql.util.PSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService implements com.cc.ccgulnara.services.Service<Customer> {

    public Logger logger = LoggerFactory.getLogger(CustomerService.class);

    private Repository customerRepository;

    public CustomerService(Repository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<Customer> getAll() {
        try {
            return customerRepository.getAll();
        } catch (RuntimeException e) {
            logger.error("Error: "+e.getMessage());
        }
        return null;
    }

    public Customer getById(int id) throws RuntimeException {
        Customer customer = new Customer();
        try {
            return (Customer) customerRepository.getById(id);
        } catch (RuntimeException e) {
            logger.error("Error: "+e.getMessage());
            customer.setCust_id(404);
            customer.setC_name("Error: ");
            customer.setC_surname("We do not have that user!");
        }
        return customer;
    }

    public void create(Customer customer) {
        try {
            if (customer.getC_name().length() < 1) {
                //exception handling
                throw new SpringException("Given name is too short");
            } else {
                try {
                    customerRepository.create(customer);
                } catch (RuntimeException e) {
                    logger.error("Error: "+e.getMessage());
                }
            }
        } catch (SpringException ex) {
            logger.error("Error: "+ex.getExceptionMsg());
        }
    }

    public void deleteById(int id) {
        try {
            customerRepository.deleteById(id);
        } catch (RuntimeException e) {
            logger.error("Error: "+e.getMessage());
        }
    }
}
