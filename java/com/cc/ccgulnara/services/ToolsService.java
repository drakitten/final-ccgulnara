package com.cc.ccgulnara.services;

import com.cc.ccgulnara.SpringException;
import com.cc.ccgulnara.models.Customer;
import com.cc.ccgulnara.models.Tools;
import com.cc.ccgulnara.repositories.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ToolsService implements com.cc.ccgulnara.services.Service<Tools> {

    public Logger logger = LoggerFactory.getLogger(CustomerService.class);

    private Repository toolsRepository;

    public ToolsService(Repository toolsRepository) {
        this.toolsRepository = toolsRepository;
    }

    public List<Tools> getAll() {
        try {
            return toolsRepository.getAll();
        } catch (RuntimeException e) {
            logger.error("Error: "+e.getMessage());
        }
        return null;
    }

    public Tools getById(int id) {
        Tools tools = new Tools();
        try {
            return (Tools) toolsRepository.getById(id);
        } catch (RuntimeException e) {
            logger.error("Error: "+e.getMessage());
            tools.setTool_group_id(404);
            tools.setTool_group_name("We do not have that tools!");
        }
        return tools;
    }

    public void create(Tools tools) {
        try {
            if (tools.getTool_group_name().length() < 1) {
                //exception handling
                throw new SpringException("Given name is too short");
            } else {
                try {
                    toolsRepository.create(tools);
                } catch (RuntimeException e) {
                    logger.error("Error: "+e.getMessage());
                }
            }
        } catch (SpringException ex) {
            logger.error("Error: "+ex.getExceptionMsg());
        }
    }

    public void deleteById(int id) {
        try {
            toolsRepository.deleteById(id);
        } catch (RuntimeException e) {
            logger.error("Error: "+e.getMessage());
        }
    }
}
