package com.cc.ccgulnara.services;

import com.cc.ccgulnara.SpringException;
import com.cc.ccgulnara.models.Chemicals;
import com.cc.ccgulnara.repositories.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChemicalsService implements com.cc.ccgulnara.services.Service<Chemicals> {

    public Logger logger = LoggerFactory.getLogger(CustomerService.class);

    private Repository chemicalsRepository;

    public ChemicalsService(Repository chemicalsRepository) {
        this.chemicalsRepository = chemicalsRepository;
    }

    public List<Chemicals> getAll() {
        try {
            return chemicalsRepository.getAll();
        } catch (RuntimeException e) {
            logger.error("Error: "+e.getMessage());
        }
        return null;
    }

    public Chemicals getById(int id) {
        Chemicals chemicals = new Chemicals();
        try {
            return (Chemicals) chemicalsRepository.getById(id);
        } catch (RuntimeException e) {
            logger.error("Error: "+e.getMessage());
            chemicals.setCh_group_id(404);
            chemicals.setCh_group_name("We do not have chemicals with that id!");
        }
        return chemicals;
    }

    public void create(Chemicals chemicals) {
        try {
            if (chemicals.getCh_group_name().length() < 1) {
                //exception handling
                throw new SpringException("Given name is too short");
            } else {
                try {
                    chemicalsRepository.create(chemicals);
                } catch (RuntimeException e) {
                    logger.error("Error: "+e.getMessage());
                }
            }
        } catch (SpringException ex) {
            logger.error("Error: "+ex.getExceptionMsg());
        }
    }

    public void deleteById(int id) {
        try {
            chemicalsRepository.deleteById(id);
        } catch (RuntimeException e) {
            logger.error("Error: "+e.getMessage());
        }
    }
}
