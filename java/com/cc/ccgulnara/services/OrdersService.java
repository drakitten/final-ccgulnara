package com.cc.ccgulnara.services;

import com.cc.ccgulnara.models.Orders;
import com.cc.ccgulnara.repositories.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrdersService implements com.cc.ccgulnara.services.Service<Orders> {

    public Logger logger = LoggerFactory.getLogger(CustomerService.class);

    private Repository ordersRepository;

    public OrdersService(Repository ordersRepository) {
        this.ordersRepository = ordersRepository;
    }

    public List<Orders> getAll() {
        try {
            return ordersRepository.getAll();
        } catch (RuntimeException e) {
            logger.error("Error: "+e.getMessage());
        }
        return null;
    }

    public Orders getById(int id) {
        Orders orders = new Orders();
        try {
            return (Orders) ordersRepository.getById(id);
        } catch (RuntimeException e) {
            logger.error("Error: "+e.getMessage());
            orders.setOrder_id(404);
        }
        return orders;
    }

    public void create(Orders orders) {
        try {
            ordersRepository.create(orders);
        } catch (RuntimeException e) {
            logger.error("Error: "+e.getMessage());
        }
    }

    public void deleteById(int id) {
        try {
            ordersRepository.deleteById(id);
        } catch (RuntimeException e) {
            logger.error("Error: "+e.getMessage());
        }
    }
}
