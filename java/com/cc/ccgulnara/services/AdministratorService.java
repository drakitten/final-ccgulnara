package com.cc.ccgulnara.services;

import com.cc.ccgulnara.SpringException;
import com.cc.ccgulnara.models.Administrator;
import com.cc.ccgulnara.repositories.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AdministratorService implements com.cc.ccgulnara.services.Service<Administrator> {

    public Logger logger = LoggerFactory.getLogger(CustomerService.class);

    private Repository administratorRepository;

    public AdministratorService(Repository administratorRepository) {
        this.administratorRepository = administratorRepository;
    }

    public List<Administrator> getAll() {
        try {
            return administratorRepository.getAll();
        } catch (RuntimeException e) {
            logger.error("Error: "+e.getMessage());
        }
        return null;
    }

    public Administrator getById(int id) {
        Administrator administrator = new Administrator();
        try {
            return (Administrator) administratorRepository.getById(id);
        } catch (RuntimeException e) {
            logger.error("Error: "+e.getMessage());
            administrator.setAdmin_id(404);
            administrator.setA_name("Error:");
            administrator.setA_surname("We do not have administrator with that id!");
        }
        return administrator;
    }

    public void create(Administrator administrator) {
        try {
            if (administrator.getA_name().length() < 1) {
                //exception handling
                throw new SpringException("Given name is too short");
            } else {
                try {
                    administratorRepository.create(administrator);
                } catch (RuntimeException e) {
                    logger.error("Error: "+e.getMessage());
                }
            }
        } catch (SpringException ex) {
            logger.error("Error: "+ex.getExceptionMsg());
        }
    }

    public void deleteById(int id) {
        try {
            administratorRepository.deleteById(id);
        } catch (RuntimeException e) {
            logger.error("Error: "+e.getMessage());
        }
    }
}
