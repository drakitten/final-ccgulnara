package com.cc.ccgulnara;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CcgulnaraApplication {

	public static void main(String[] args) {

		SpringApplication.run(CcgulnaraApplication.class, args);
	}
}
