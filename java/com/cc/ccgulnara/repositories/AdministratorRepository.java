package com.cc.ccgulnara.repositories;

import com.cc.ccgulnara.SpringException;
import com.cc.ccgulnara.models.Administrator;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class AdministratorRepository implements com.cc.ccgulnara.repositories.Repository<Administrator> {

    private JdbcTemplate jdbcTemplate;

    public AdministratorRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Administrator> getAll() {
        String sql = "SELECT * FROM administrator";
        return jdbcTemplate.query(sql, new RowMapper<Administrator>() {
            @Override
            public Administrator mapRow(ResultSet resultSet, int i) throws SQLException {
                Administrator administrator = new Administrator();
                administrator.setAdmin_id(resultSet.getInt(1));
                administrator.setA_name(resultSet.getString(2));
                administrator.setA_surname(resultSet.getString(3));
                return administrator;
            }
        });
    }

    public Administrator getById(int id) {
        String sql = "SELECT * FROM administrator WHERE admin_id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, new BeanPropertyRowMapper<>(Administrator.class));
    }

    public void create(Administrator administrator) {
        String sql = "INSERT INTO administrator VALUES (?, ?, ?)";
        jdbcTemplate.update(sql, administrator.getAdmin_id(), administrator.getA_name(), administrator.getA_surname());
    }

    public void deleteById(int id) {
        String sql = "DELETE FROM administrator WHERE admin_id = ?";
        jdbcTemplate.update(sql, id);
    }
}
