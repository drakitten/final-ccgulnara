package com.cc.ccgulnara.repositories;

import com.cc.ccgulnara.SpringException;
import com.cc.ccgulnara.models.Customer;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class CustomerRepository implements com.cc.ccgulnara.repositories.Repository<Customer> {

    private final JdbcTemplate jdbcTemplate;

    public CustomerRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Customer> getAll() {
        String sql = "SELECT * FROM customer";
        return jdbcTemplate.query(sql, new RowMapper<Customer>() {
            @Override
            public Customer mapRow(ResultSet resultSet, int i) throws SQLException {
                Customer customer = new Customer();
                customer.setCust_id(resultSet.getInt(1));
                customer.setC_name(resultSet.getString(2));
                customer.setC_surname(resultSet.getString(3));
                return customer;
            }
        });
    }

    public Customer getById(int id) {
        String sql = "SELECT * FROM customer WHERE cust_id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, new BeanPropertyRowMapper<>(Customer.class));
    }

    public void create(Customer customer) {
        String sql = "INSERT INTO customer VALUES (?, ?, ?)";
        jdbcTemplate.update(sql, customer.getCust_id(), customer.getC_name(), customer.getC_surname());
    }

    public void deleteById(int id) {
        String sql = "DELETE FROM customer WHERE cust_id = ?";
        jdbcTemplate.update(sql, id);
    }
}
