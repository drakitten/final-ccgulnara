package com.cc.ccgulnara.repositories;

import com.cc.ccgulnara.SpringException;
import com.cc.ccgulnara.models.Tools;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class ToolsRepository implements com.cc.ccgulnara.repositories.Repository<Tools> {

    private JdbcTemplate jdbcTemplate;

    public ToolsRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Tools> getAll() {
        String sql = "SELECT * FROM tools";
        return jdbcTemplate.query(sql, new RowMapper<Tools>() {
            @Override
            public Tools mapRow(ResultSet resultSet, int i) throws SQLException {
                Tools tools = new Tools();
                tools.setTool_group_id(resultSet.getInt(1));
                tools.setTool_group_name(resultSet.getString(2));
                return tools;
            }
        });
    }

    public Tools getById(int id) {
        String sql = "SELECT * FROM tools WHERE tool_group_id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, new BeanPropertyRowMapper<>(Tools.class));
    }

    public void create(Tools tools) {
        String sql = "INSERT INTO tools VALUES (?, ?)";
        jdbcTemplate.update(sql, tools.getTool_group_id(), tools.getTool_group_name());

    }

    public void deleteById(int id) {
        String sql = "DELETE FROM tools WHERE tool_group_id = ?";
        jdbcTemplate.update(sql, id);
    }
}
