package com.cc.ccgulnara.repositories;

import com.cc.ccgulnara.SpringException;
import com.cc.ccgulnara.models.Service;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class ServiceRepository implements com.cc.ccgulnara.repositories.Repository<Service> {

    private JdbcTemplate jdbcTemplate;

    public ServiceRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Service> getAll() {
        String sql = "SELECT * FROM service";
        return jdbcTemplate.query(sql, new RowMapper<Service>() {
            @Override
            public Service mapRow(ResultSet resultSet, int i) throws SQLException {
                Service service = new Service();
                service.setService_id(resultSet.getInt(1));
                service.setS_name(resultSet.getString(2));
                service.setDuration_hours(resultSet.getFloat(3));
                service.setCh_group_id(resultSet.getInt(4));
                service.setTool_group_id(resultSet.getInt(5));
                return service;
            }
        });
    }

    public Service getById(int id) {
        String sql = "SELECT * FROM service WHERE service_id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, new BeanPropertyRowMapper<>(Service.class));
    }

    public void create(Service service) {
        String sql = "INSERT INTO service VALUES (?, ?, ?, ?, ?)";
        jdbcTemplate.update(sql, service.getService_id(), service.getS_name(), service.getDuration_hours(),
                    service.getCh_group_id(), service.getTool_group_id());
    }

    public void deleteById(int id) {
        String sql = "DELETE FROM service WHERE service_id = ?";
        jdbcTemplate.update(sql, id);
    }


}
