package com.cc.ccgulnara.repositories;

import java.util.List;

public interface Repository<T> {

    List<T> getAll();
    <T> T getById(int id);
    void create(T item);
    void deleteById(int id);
}
