package com.cc.ccgulnara.repositories;

import com.cc.ccgulnara.models.Address;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class AddressRepository implements com.cc.ccgulnara.repositories.Repository<Address> {

    private final JdbcTemplate jdbcTemplate;

    public AddressRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Address> getAll() {
        String sql = "SELECT * FROM address";
        return jdbcTemplate.query(sql, new RowMapper<Address>() {
            @Override
            public Address mapRow(ResultSet resultSet, int i) throws SQLException {
                Address address = new Address();
                address.setCust_id(resultSet.getInt(1));
                address.setAddress_id(resultSet.getInt(2));
                address.setCountry(resultSet.getString(3));
                address.setCity(resultSet.getString(4));
                address.setStreet(resultSet.getString(5));
                address.setHouse(resultSet.getInt(6));
                address.setApartment(resultSet.getInt(7));
                return address;
            }
        });
    }

    public Address getById(int id) {
        String sql = "SELECT * FROM address WHERE address_id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, new BeanPropertyRowMapper<>(Address.class));
    }

    public void create(Address address) {
        int id = address.getCust_id();
        String sql = "INSERT INTO address VALUES (?, ?, ?, ?, ?, ?, ?)";
        jdbcTemplate.update(sql, address.getCust_id(), address.getAddress_id(), address.getCountry(), address.getCity(),
                address.getStreet(), address.getHouse(), address.getApartment());

    }

    public void deleteById(int id) {
        String sql = "DELETE FROM address WHERE address_id = ?";
        jdbcTemplate.update(sql, id);
    }
}
