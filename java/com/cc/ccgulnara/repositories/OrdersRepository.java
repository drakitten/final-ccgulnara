package com.cc.ccgulnara.repositories;

import com.cc.ccgulnara.SpringException;
import com.cc.ccgulnara.models.Customer;
import com.cc.ccgulnara.models.Orders;
import com.sun.org.apache.xpath.internal.operations.Or;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class OrdersRepository implements com.cc.ccgulnara.repositories.Repository<Orders> {

    private JdbcTemplate jdbcTemplate;

    public OrdersRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Orders> getAll() {
        String sql = "SELECT * FROM orders";
        return jdbcTemplate.query(sql, new RowMapper<Orders>() {
            @Override
            public Orders mapRow(ResultSet resultSet, int i) throws SQLException {
                Orders orders = new Orders();
                orders.setOrder_id(resultSet.getInt(1));
                orders.setCust_id(resultSet.getInt(2));
                orders.setAdmin_id(resultSet.getInt(3));
                orders.setService_id(resultSet.getInt(4));
                orders.setStatus(resultSet.getInt(5));
                orders.setDate(resultSet.getDate(6));
                return orders;
            }
        });
    }
    public Orders getById(int id) {
        String sql = "SELECT * FROM orders WHERE order_id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, new BeanPropertyRowMapper<>(Orders.class));
    }

    public void create(Orders orders) {
        String sql = "INSERT INTO orders VALUES (?, ?, ?, ?, ?, ?)";
        jdbcTemplate.update(sql, orders.getOrder_id(), orders.getCust_id(), orders.getAdmin_id(), orders.getService_id(),
                orders.getStatus(), orders.getDate());
    }

    public void deleteById(int id) {
        String sql = "DELETE FROM orders WHERE order_id = ?";
        jdbcTemplate.update(sql, id);
    }
}
