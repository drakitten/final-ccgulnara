package com.cc.ccgulnara.repositories;

import com.cc.ccgulnara.SpringException;
import com.cc.ccgulnara.models.Chemicals;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class ChemicalsRepository implements com.cc.ccgulnara.repositories.Repository<Chemicals> {

    private JdbcTemplate jdbcTemplate;

    public ChemicalsRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Chemicals> getAll() {
        String sql = "SELECT * FROM chemicals";
        return jdbcTemplate.query(sql, new RowMapper<Chemicals>() {
            @Override
            public Chemicals mapRow(ResultSet resultSet, int i) throws SQLException {
                Chemicals chemicals = new Chemicals();
                chemicals.setCh_group_id(resultSet.getInt(1));
                chemicals.setCh_group_name(resultSet.getString(2));
                return chemicals;
            }
        });
    }

    public Chemicals getById(int id) {
        String sql = "SELECT * FROM chemicals WHERE ch_group_id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, new BeanPropertyRowMapper<>(Chemicals.class));
    }

    public void create(Chemicals chemicals) {
        String sql = "INSERT INTO chemicals VALUES (?, ?)";
        jdbcTemplate.update(sql, chemicals.getCh_group_id(), chemicals.getCh_group_name());
    }

    public void deleteById(int id) {
        String sql = "DELETE FROM chemicals WHERE ch_group_id = ?";
        jdbcTemplate.update(sql, id);
    }
}
